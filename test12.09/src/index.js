import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Provider} from "react-redux";
import {createStore, combineReducers, applyMiddleware} from 'redux';
import {reducer as reduxFormReducer} from 'redux-form';
import createSagaMiddleware from 'redux-saga'
import mySaga from './saga';
import data from './reducers'

const sagaMiddleware = createSagaMiddleware();


const reducer = combineReducers({
    data,
    form: reduxFormReducer, // mounted under "form"
});

let store = createStore(
    reducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(sagaMiddleware),
);

sagaMiddleware.run(mySaga);

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root'));


