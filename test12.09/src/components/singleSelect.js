import React from "react";
import Select from 'react-select';

const renderSelect = (props) => {
    const { input, options } = props;

    let optionsForRender = [];
    options.forEach(item => {
        let nextOption = {
            label : item,
            value : item
        };
        optionsForRender.push(nextOption);
    });

    return (
        <Select
            {...props}
            value={input.value}
            options={optionsForRender}
            onChange={(value) => input.onChange(value)}
            onBlur={() => input.onBlur(input.value)}
        />
    )
};

export default renderSelect;
