import React from "react";
import {connect} from 'react-redux';
import { search } from "../actions/getData";

const searchField = (props) => (
    <input {...props.input} type="text" />
);

const mapDispatchToProps = {
    search
};

export default connect(null, mapDispatchToProps)(searchField);
