import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';
import SingleSelect from './singleSelect';
import MultiSelect from './multiSelect';
import RenderField from './renderField';
import {search, multiSearch} from "../actions/getData";

const validate = values => {
  const errors = {};

  if (!values.email) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }

  if (!values.firstname) {
    errors.firstname = 'Required'
  }

  if (!values.text) {
    errors.text = 'You should enter something...'
  }

  if (!/^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}/.test(values.phone)) {
    errors.tel = 'Invalid phone number'
  }

  return errors
};

function initialData(state) {
  let initialData = {};
  state.data.data.forEach(item => {
    if (item.name !== undefined) {
      initialData[item.name.toLowerCase()] = item.value || '';
    }
  });

  return initialData;
}


class InitializeFromStateForm extends PureComponent {

  render() {
    const {handleSubmit, data, pristine, reset, submitting, search, resultOfSearch, resultForMultiInput, multiSearch, formValues} = this.props;
    return (
      <div className="container">
        <form onSubmit={handleSubmit}>

          {data.map(item => {

            switch (item.type) {
              case 'text': {
                if (item.name === "search") {
                  return (
                    <div key={item.name} className={`${item.type} input`}>
                      <label>{item.name}</label>
                      <Field
                        name={item.name.toLowerCase()}
                        component={RenderField}
                        type={item.type}
                        onChange={(e, newValue) => {
                          search(newValue);
                        }}
                        onBlur={(e) => e.preventDefault()}
                      />
                      <div className="resultsOfSearch" key='result'>
                        {resultOfSearch ? resultOfSearch.map((item) => {
                          return (<div key={`result ${item.name}`}>{item.name}</div>)
                        }) : null}
                      </div>
                    </div>
                  )
                }
                else if (item.name === "multiInput") {
                  return (
                    <div key={item.name} className={`${item.type} input`}>
                      <label>{item.name}</label>
                      <Field
                        name={item.name.toLowerCase()}
                        component={RenderField}
                        type={item.type}
                        onChange={(e, newValue) => {
                          multiSearch(newValue);
                        }}
                        onBlur={(e) => e.preventDefault()}
                      />
                      <div className="checkbox">
                        <label>Show more?</label>
                        <Field
                          name="showMore"
                          component="input"
                          type="checkbox"
                          value={item.showDepended}
                        />
                        {formValues.values.showMore ? <div className="resultForMultiInput" key='result'>
                          {resultForMultiInput ? resultForMultiInput.map((item) => {
                            return (<div key={`result ${item.name}`}>{item.name}</div>)
                          }) : null}
                        </div> : null}
                      </div>

                    </div>
                  )
                }
                else {
                  return (
                    <div key={item.name} className={`${item.type} input`}>
                      <label>{item.name}</label>
                      <Field
                        name={item.name.toLowerCase()}
                        component={RenderField}
                        type={item.type}
                      />
                    </div>
                  )
                }
              }
              case 'radio': {
                return (
                  <div key={item.name} className={`${item.type} input`}>
                    <label>{item.name}</label>
                    {item.value.map(value => {
                      return (
                        <label key={value}>
                          <Field
                            name={item.name.toLowerCase()}
                            component="input"
                            type="radio"
                            value={value}
                          />{` ${value}`}
                        </label>)
                    })
                    }
                  </div>
                )
              }
              case 'singleSelect' : {
                return (
                  <div key={item.name} className={`${item.type} input`}>
                    <label>{item.name}</label>
                    <div>
                      <Field
                        name={item.name}
                        component={SingleSelect}
                        options={item.options}
                      />
                    </div>
                  </div>
                )
              }
              case 'multiSelect' : {
                return (
                  <div key={item.name} className={`${item.type} input`}>
                    <label>{item.name}</label>
                    <div>
                      <Field
                        name={item.name}
                        component={MultiSelect}
                        options={item.options}
                      />
                    </div>
                  </div>
                )
              }
              case 'textarea' : {
                return (
                  <div key={item.name} className={`${item.type} input`}>
                    <label>{item.name}</label>
                    <div>
                      <Field
                        name={item.name.toLowerCase()}
                        component="textarea"
                      />
                    </div>
                  </div>
                )
              }
              case 'checkbox' : {
                return (
                  <div key={item.name} className={`${item.type} input`}>
                    <label htmlFor={item.name}>{item.name}</label>
                    <Field
                      name={item.name.toLowerCase()}
                      id={item.name}
                      component="input"
                      type="checkbox"
                    />
                  </div>
                )
              }
              case 'file' : {
                return (
                  <div key={`${item.name}+${item.type}`} className={`${item.type} input`}>
                    <label>{item.name}</label>
                    <input type='file'/>
                  </div>
                )
              }
              default : {
                return (
                  <div key={`${item.name}+${item.type}`} className={`${item.type} input`}>
                    <label>{item.name || item.type}</label>
                    <Field
                      name={item.name ? item.name.toLowerCase() : item.type}
                      component={RenderField}
                      type={item.type}
                    />
                  </div>
                )
              }
            }

          })
          }
          <div className='buttons'>
            <button type="submit" disabled={pristine || submitting}>
              Submit
            </button>
            <button type="button" disabled={pristine || submitting} onClick={reset}>
              Undo Changes
            </button>

          </div>
        </form>
      </div>
    )
  };
}

// Decorate with reduxForm(). It will read the initialValues prop provided by connect()
InitializeFromStateForm = reduxForm({
  form: 'form', // a unique identifier for this form
  validate,
  enableReinitialize: true
})(InitializeFromStateForm);

const mapStateToProps = state => {
  return {
    initialValues: initialData(state),
    data: state.data.data,
    resultOfSearch: state.data.resultOfSearch,
    resultForMultiInput: state.data.resultForMultiInput,
    formValues: state.form.form
  }
};

const mapDispatchToProps = {
  search,
  multiSearch
};

// You have to connect() to any reducers that you wish to connect to yourself
InitializeFromStateForm = connect(mapStateToProps, mapDispatchToProps)(InitializeFromStateForm);


export default InitializeFromStateForm;


