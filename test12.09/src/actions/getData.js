import { LOAD_DATA, SEARCH, SEARCH_FOR_MULTI } from "./actions";


export function loadData () {
    return {
        type: LOAD_DATA
    }
};

export function search(value) {
    return {
        type: SEARCH,
        data: value
    }
}
export function multiSearch(value) {
    return {
        type: SEARCH_FOR_MULTI,
        data: value
    }
}


