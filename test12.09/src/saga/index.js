import {call, put, takeEvery, throttle} from 'redux-saga/effects';
import {
    LOAD_DATA,
    LOAD_DATA_SUCCEEDED,
    LOAD_DATA_FAILED,
    SEARCH,
    SEARCH_SUCCEEDED,
    SEARCH_FAILED,
    SEARCH_EMPTY,
    SEARCH_FOR_MULTI,
    SEARCH_FOR_MULTI_SUCCEEDED,
    SEARCH_FOR_MULTI_FAILED,
    SEARCH_FOR_MULTI_EMPTY
} from "../actions/actions";

const axios = require('axios');

function* getData() {
    try {
        const response = yield call(axios, {
            url: `/data`,
            method: 'get',
        });
        if (response.status === 200 || response.status === 304) {
            let data = [...response.data];
            yield put({type: LOAD_DATA_SUCCEEDED, data});
        } else throw new Error(response)
    }
    catch (e) {
        yield put({type: LOAD_DATA_FAILED, message: e});
    }
}

function* search(action) {
    const simpleSearch = (action.type === SEARCH);
    const multiSearch = (action.type === SEARCH_FOR_MULTI);

    if (action.data !== undefined) {
        try {
            const response = yield call(axios, {
                url: `/search`,
                method: 'get',
                params: {
                    value: action.data.trim(),
                }
            });
            if (response.status === 200 || response.status === 304 || response.status === 204) {
                let data = [...response.data];
                if (simpleSearch) {
                    yield put({type: SEARCH_SUCCEEDED, data: data});
                }
                if (multiSearch) {
                    yield put({type: SEARCH_FOR_MULTI_SUCCEEDED, data: data});
                }
            } else throw new Error(response);

            if (response.status === 404 && simpleSearch) {
                yield put({type: SEARCH_FAILED})
            }
            if (response.status === 404 && multiSearch) {
                yield put({type: SEARCH_FOR_MULTI_FAILED})
            }
        }
        catch (e) {
            yield put({type: SEARCH_FAILED, message: e});
        }
    }
    if (((action.data === "") || (action.data === undefined)) && simpleSearch) {
        yield put({type: SEARCH_EMPTY});
    }
    if (((action.data === "") || (action.data === undefined)) && multiSearch) {
        yield put({type: SEARCH_FOR_MULTI_EMPTY});
    }

}

function* mySaga() {
    yield takeEvery(LOAD_DATA, getData);
    yield throttle(1000, SEARCH, search);
    yield throttle(1000, SEARCH_FOR_MULTI, search);
}

export default mySaga;
