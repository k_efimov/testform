import React, {Component} from 'react';
import {connect} from 'react-redux'
import { loadData } from './actions/getData';
import './App.css';
import Form from './components/form';



class App extends Component {
    componentDidMount() {
        this.props.loadData();
    }

    render() {
        return (
            <Form/>
        )
    }
}

const mapDispatchToProps = {
    loadData
};

export default connect(null, mapDispatchToProps)(App)

