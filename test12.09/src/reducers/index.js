import {LOAD_DATA_SUCCEEDED, SEARCH_SUCCEEDED, SEARCH_EMPTY, SEARCH_FOR_MULTI_SUCCEEDED, SEARCH_FOR_MULTI_EMPTY} from "../actions/actions";

const initialState = {
    data: [],
    resultOfSearch: [],
    resultForMultiInput: []
};

export default function users(state = initialState, action) {
    switch (action.type) {
        case LOAD_DATA_SUCCEEDED:
            return {
                ...state,
                data: action.data
            };

        case SEARCH_SUCCEEDED:
            return {
                ...state,
                resultOfSearch: action.data
            };
        case SEARCH_EMPTY:
            return {
                ...state,
                resultOfSearch: []
            };
        case SEARCH_FOR_MULTI_SUCCEEDED:
            return {
                ...state,
                resultForMultiInput: action.data
            };
        case SEARCH_FOR_MULTI_EMPTY:
            return {
                ...state,
                resultForMultiInput: []
            };

        default:
            return state;
    }
}
