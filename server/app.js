var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");

const data = [
    {"type": "text", "name": "FirstName", "value": "FirstName"},
    {
        "type": "checkbox",
        "name": "LastName",
        "value": "checked"
    },
    {
        "type": "radio",
        "name": "RadioName",
        "value": ["FirstName", "LastName"]
    }, {"type": "file"}, {"type": "email"}, {"type": "tel"}, {
        "type": "singleSelect",
        "name": "SingleOptions",
        "options": ["1", "2", "3"]
    }, {"type": "multiSelect", "name": "MultiOptions", "options": ["first option", "second option", "third option"]},
    {"type": "text", "name": "search"},
    {"type" : "text", "name": "multiInput", "showDepended": false}
];

const dataForSearch = [
    {name: "Ivan"},
    {name: "Pole"},
    {name: "Name"},
    {name: "Action"},
    {name: "Free"},

];


var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.get("/data", function (req, res, next) {
    res.send(JSON.stringify(data));
});
app.get("/search", function (req, res, next) {
    let searchableText = req.query.value;
    if (searchableText) {
        let result = dataForSearch.filter(item => {
            return item.name.toLowerCase().includes(searchableText.trim().toLowerCase());
        });
        if (result[0]) res.send(JSON.stringify(result));
        res.sendStatus(204);
    } else res.sendStatus(202);
});


module.exports = app;
